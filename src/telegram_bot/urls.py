from django.conf.urls import include, url
from django.contrib import admin

from django.views.generic import TemplateView

urlpatterns = [
    # Examples:
    # url(r'^$', 'telegram_bot.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name="home.html"), name='home'),
    url(r'^telegram/', include('telegram_bot.urls', namespace='telegram')),
]
